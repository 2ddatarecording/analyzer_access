"""
 Copyright (c) 2021 2D Debus & Diebold Meßsysteme GmbH
 Author Pascal Hain
"""
import os
import sys
import platform
from ctypes import byref, CDLL, c_bool, c_double, c_char_p, c_ubyte, c_byte, c_ushort, c_short, c_long, \
    c_ulong, c_ulonglong, c_longlong, c_float, create_string_buffer, POINTER, sizeof
import logging
import numpy as np


class AnalyzerAccess:
    def __init__(self, meas_path=None):

        self.event_name = None
        self.mes_name = None
        self.mes_dir = None
        self.ddd_name = None
        self.channels = None
        self.main_sampling_rate = None
        self.scans_overall = None

        if platform.architecture()[0] == '64bit':
            self.dll = CDLL(os.path.join(os.environ['2D_MTEPath'], 'MESPython64.dll'))
        else:
            self.dll = CDLL(os.path.join(os.environ['2D_MTEPath'], 'MESPython.dll'))

        if meas_path is None:
            self.__select_measurement()
        else:
            self.mes_dir = bytes((meas_path + '\\'), 'cp1252')
            self.event_name = bytes(os.path.dirname(meas_path), 'cp1252')
            self.mes_name = bytes(os.path.basename(meas_path), 'cp1252')
            self.ddd_name = bytes(str(os.path.split(os.path.basename(meas_path))[1])[:-3] + 'DDD', 'cp1252')

        if self.mes_dir is not None:
            self.__init_measurement()

    def __del__(self):
        self.dll.ExitMeasurement()

    def __select_measurement(self) -> bool:
        func = self.dll.SelectMeasurement
        func.argtypes = [c_char_p, c_char_p,
                         c_char_p, c_char_p]
        func.restype = c_bool
        event_name = create_string_buffer(255)
        mes_name = create_string_buffer(255)
        mes_dir = create_string_buffer(255)
        ddd_name = create_string_buffer(255)

        try:
            ret = func(event_name, mes_name, mes_dir, ddd_name)

            if ret is True:
                self.event_name = event_name.value
                self.mes_name = mes_name.value
                self.mes_dir = mes_dir.value
                self.ddd_name = ddd_name.value
                return ret
            else:
                sys.exit(0)
        except Exception as ex:
            logging.debug(str(ex))
            sys.exit(1)

    def __init_measurement(self) -> bool:
        func = self.dll.InitMeasurement
        func.argtypes = [c_char_p, POINTER(c_ulong),
                         c_char_p, POINTER(c_ulong)]
        func.restype = c_bool

        mes_path = create_string_buffer(self.mes_dir[:-1])
        mes_main_sampling_rate = c_ulong(0)
        mes_channel_list = create_string_buffer(32 * 1024 * 1024)
        scans_overall = c_ulong(0)

        try:
            ret = func(mes_path, byref(mes_main_sampling_rate), mes_channel_list, byref(scans_overall))

            if ret is True:
                self.main_sampling_rate = int(mes_main_sampling_rate.value)
                self.scans_overall = int(scans_overall.value)
                self.channels = []
                mes_channel_list = str(mes_channel_list.value, 'cp1252')

                channel_split = mes_channel_list.split(',')
                for channel in channel_split:
                    channel = channel.split(';')
                    if str(channel[0]).isidentifier():
                        self.channels.append({'name': channel[0], 'unit': channel[1],
                                              'ratio': channel[2], 'type': channel[3],
                                              'multiplier': channel[4], 'offset': channel[5]})
                    else:
                        logging.critical('Channel Name: ' + channel[0] + ' is not a valid identifier')
            return ret
        except Exception as init_exception:
            logging.debug(str(init_exception))
            sys.exit(1)

    def get_lap_count(self) -> int:
        func = self.dll.GetLapCount
        func.argtypes = ()
        func.restype = c_ulong

        try:
            return func()
        except Exception as ex:
            logging.debug(str(ex))

    def get_channel_data_org_type(self, name: str, data_type: str = None,
                                  ratio: int = 1, scan_from: int = 0, scan_to: int = -1) -> object:

        if scan_to == -1:
            scan_to = self.scans_overall - 1

        func = self.dll.GetChannelDataOrgType
        func.restype = c_ulong

        _name = create_string_buffer(name.encode('cp1252'))
        vector_count = c_ulong(int((scan_to - scan_from + 1) / ratio))
        if data_type == 'uint8':
            func.argtypes = [c_char_p, c_long, c_long, c_ulong, POINTER(c_ubyte)]
            channel_data_vector = (c_ubyte * vector_count.value)()
        elif data_type == 'uint16':
            func.argtypes = [c_char_p, c_long, c_long, c_ulong, POINTER(c_ushort)]
            channel_data_vector = (c_ushort * vector_count.value)()
            logging.debug(sizeof(channel_data_vector))
            logging.debug(hex(id(channel_data_vector)))
        elif data_type == 'uint32':
            func.argtypes = [c_char_p, c_long, c_long, c_ulong, POINTER(c_ulong)]
            channel_data_vector = (c_ulong * vector_count.value)()
        elif data_type == 'uint64':
            func.argtypes = [c_char_p, c_long, c_long, c_ulong, POINTER(c_ulonglong)]
            channel_data_vector = (c_ulonglong * vector_count.value)()
        elif data_type == 'int8':
            func.argtypes = [c_char_p, c_long, c_long, c_ulong, POINTER(c_byte)]
            channel_data_vector = (c_byte * vector_count.value)()
        elif data_type == 'int16':
            func.argtypes = [c_char_p, c_long, c_long, c_ulong, POINTER(c_short)]
            channel_data_vector = (c_short * vector_count.value)()
        elif data_type == 'int32':
            func.argtypes = [c_char_p, c_long, c_long, c_ulong, POINTER(c_long)]
            channel_data_vector = (c_long * vector_count.value)()
        elif data_type == 'int64':
            func.argtypes = [c_char_p, c_long, c_long, c_ulong, POINTER(c_longlong)]
            channel_data_vector = (c_longlong * vector_count.value)()
        elif data_type == 'real32':
            func.argtypes = [c_char_p, c_long, c_long, c_ulong, POINTER(c_float)]
            channel_data_vector = (c_float * vector_count.value)()
        else:
            func.argtypes = [c_char_p, c_long, c_long, c_ulong, POINTER(c_double)]
            channel_data_vector = (c_double * vector_count.value)()

        scan_from = c_long(scan_from)
        scan_to = c_long(scan_to)

        try:
            logging.debug('Get channel data original data type: ' + name + ', Data type: ' + data_type)

            ret = func(_name, scan_from, scan_to, vector_count, channel_data_vector)
            if ret != 0:
                if data_type.startswith('real32'):
                    data_type = 'float'
                elif data_type.startswith('real64'):
                    data_type = 'double'
                samples = np.array(channel_data_vector, np.dtype(data_type))
                logging.debug(samples)
                return samples
            else:
                logging.debug('GetChannelData from Channel ' + name + ' returns 0')
                return None

        except Exception as ex:
            logging.debug(str(ex))

    def get_lap_times(self, count: int) -> list:
        func = self.dll.GetLapTimes
        func.argtypes = [c_ulong, POINTER(c_ulong)]
        func.restype = c_bool

        times = (c_ulong * count)()
        count = c_ulong(count)

        try:
            ret = func(count, times)
            if ret is not False:
                return list(times)
            else:
                logging.critical('GetLapTimes returns 0')
                return []

        except Exception as ex:
            logging.debug(str(ex))

    def get_spec_sheet_groups(self) -> list:
        func = self.dll.GetSpecSheetGroups
        func.argtypes = [c_char_p]
        func.restype = c_bool

        groups = create_string_buffer(65536)

        try:
            ret = func(groups)
            if ret is not False:
                groups = str(groups.value)[2:]
                groups = groups.split(';')
                return list(groups)
            else:
                logging.critical('GetSpecSheetGroups returns 0')
                return []

        except Exception as ex:
            logging.debug(str(ex))

    def get_spec_sheet_entries(self, group: str) -> list:
        func = self.dll.GetSpecSheetEntries
        func.argtypes = [c_char_p, c_char_p]
        func.restype = c_bool

        group = create_string_buffer(bytes(group.encode('utf-8')))
        spec_sheet_entries = create_string_buffer(65536)

        try:
            ret = func(group, spec_sheet_entries)
            if ret is not False:
                spec_sheet_entries = str(spec_sheet_entries.value)[2:]
                spec_sheet_entries = spec_sheet_entries.split(';')
                return list(spec_sheet_entries)
            else:
                logging.critical('GetSpecSheetEntries returns 0')
                return []

        except Exception as ex:
            logging.debug(str(ex))

    def get_spec_sheet_value(self, group: str, entry: str) -> str:
        func = self.dll.GetSpecSheetValue
        func.argtypes = [c_char_p, c_char_p, c_char_p]
        func.restype = c_bool

        group = create_string_buffer(bytes(group.encode('utf-8')))
        entry = create_string_buffer(bytes(entry.encode('utf-8')))
        value = create_string_buffer(65536)

        try:
            ret = func(group, entry, value)
            if ret is not False:
                return str(value.value)
            else:
                logging.critical('GetSpecSheetValue returns 0')
                return ''

        except Exception as ex:
            logging.debug(str(ex))
